let birds = [...document.querySelectorAll('.bird')];
let moveRight = 0;
let moveDown = 0;
let sound = document.querySelector('.sound');
const audioGame = new Audio ('/assets/audio/1.mp3');
let playSound = false;
const play = document.getElementById('play');
let score = 0;
let lives = 30;
let shots = 100;
let progressWidth = 1;
let playGame = true;

function shootGun (){
    const audio = new Audio ('/assets/audio/shootguns.mp3');
    audio.play();
};

function shootGunDie (){
    const audios = [new Audio ('/assets/audio/gun_fire.mp3'),new Audio ('/assets/audio/gun_fire2.mp3'),new Audio ('/assets/audio/shootguns.mp3')];
    audios.forEach((audio)=> audio.play());
};

function dieDuckSound (){
    const audio = new Audio ('/assets/audio/sfx_die.mp3');
    audio.play();
};

function innerSound (){
    const audio = new Audio ('/assets/audio/bg_sound.mp3');
    audio.play();
};

function soundOn(){
    playSound = true;
    sound.setAttribute('src','/assets/images/soundon.png');
    audioGame.play();
};

function soundOff(){
    sound.setAttribute('src','/assets/images/soundoff.png');
    audioGame.pause();
};

function toggleSound(){
    playSound = !playSound;
    if(playSound == true){
        soundOn();
    }
    else{
        soundOff();
    }
};

function decrementShots(){
    shots --;
    progressWidth++;
    document.querySelector('.loading').innerHTML = shots + ' %';
    let loadingBar = document.querySelector('.loading');
    let bar = document.createElement('div');
    bar.setAttribute('id','progress');
    loadingBar.appendChild(bar);
    if(progressWidth > loadingBar.style.width && shots ==0){
        document.body.removeEventListener('click',decrementShots);
    }
    else{
        bar.style.width = progressWidth + '%';
    }
};

function popupWin() {
    document.getElementById('popupWin').style.display = 'block';
    playGame = false;
    lives = 0;
    score = 150;
};

document.getElementById('close-popupWin').addEventListener('click', function() {
    document.getElementById('popupWin').style.display = 'none';
});



function move(bird) {
    if(playGame){
        let right = -50 ;
    let mDown = 0;
    document.body.style.maxHeight = window.innerHeight;
    let top = Math.floor(Math.random()* (window.innerHeight - 200));
    let bottom = Math.floor( Math.random() * (window.innerHeight - bird.clientHeight));
    bird.style.display = 'inline-flex';
    bird.style.left = right + 'px';
    bird.style.top = top + 'px';
    bird.style.bottom = bottom + 'px';

    
    let init = setInterval(() => {
        if (right >= window.innerWidth) {
            right = 0;
            top = Math.floor(Math.random()* (window.innerHeight - 200));
            bird.style.top = top + 'px';
            bottom =  Math.floor(Math.random()* (window.innerHeight - bird.clientHeight));
            bird.style.bottom = bottom + 'px' ;
        }
        right += 10;
        bird.style.marginLeft = right;
    }, 50);

    bird.addEventListener('click', function(birdshoot){
        clearInterval(init);
        let src = bird.getAttribute('src');
        shootGunDie();
        dieDuckSound();
        if(lives==0 && score==150){
            popupWin();   
        }else{
            score += 5;
            lives --;
        }
        document.querySelector('.score').innerHTML = 'score ' + score;
        document.querySelector('.lives').innerHTML = 'lives ' + lives;
        bird.setAttribute('src','/assets/images/feather.png');
        let down = setInterval(() => {
            mDown+=0.5;
            bird.style.marginTop = mDown;
        }, 5);
        setTimeout(() => {
            bird.style.display = 'none';
            clearInterval(down);
            birdshoot.target.remove();
            setTimeout(() => {
                let newBird = document.createElement('img');
                newBird.setAttribute('src',src);
                newBird.classList.add('bird');
                document.body.appendChild(newBird);
                // console.log(birds);
                move(newBird);
            }, 5000);
        }, 100);
    });
    };
    };
// window.onload = move();

sound.addEventListener('click',toggleSound);
document.body.addEventListener('click',shootGunDie);

play.onclick = function(){
    document.body.style.backgroundImage = "url('/assets/images/gamebackGround.jpg')";
    document.body.removeChild(sound);
    document.body.removeChild(play);
    let scoreText = document.createElement('p');
    scoreText.classList.add('score');
    document.body.appendChild(scoreText);
    scoreText.innerHTML = 'score ' + score;
    let livesText = document.createElement('p');
    livesText.classList.add('lives');
    document.body.appendChild(livesText);
    livesText.innerHTML = 'lives ' + lives;
    let loadingBar = document.createElement('div');
    loadingBar.classList.add('loading');
    document.body.appendChild(loadingBar);
    loadingBar.innerHTML = shots + ' %';
    let image1 = document.createElement('img');
    image1.src = "/assets/images/tree.png";
    image1.id = 'leftTree' ;
    document.body.appendChild(image1);
    let image2 = document.createElement('img');
    image2.src = "/assets/images/tree.png";
    image2.id = 'rightTree' ;
    document.body.appendChild(image2);
    
    if(playSound == true){
        soundOff();
    }
    setTimeout(() => {
            birds.forEach(bird => move(bird));
            innerSound();
            document.body.addEventListener('click',decrementShots);
    }, 2000);
};
